package model.dao;


import javafx.scene.paint.Color;
import model.tableObjects.Ebook;
import model.tableObjects.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The TagDao class manages all should be done about "database table Tag" including
 * adding a Tag, deleting a Tag, getting a Tag, getting all Tags, and editing one Tag
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-10
 */
public class TagDao {


    static enum TagStatement {
        ADD_TAG,
        EDIT_TAG,
        DELETE_TAG,
        FIND_TAG_BY_LABEL_COLOR,
        JOIN_BOOK_BOOKTAG,
        JOIN_TAG_BOOKTAG,
        FIND_ALL_TAGS,
        FIND_ALL_TAGNAMES,
        ADD_TO_BOOKTAG
    }

    private Connection connection;
    private Map<TagDao.TagStatement, PreparedStatement> statements = new HashMap<>();

    public TagDao(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException();
        }
        this.connection = connection;
        prepareStatements();
    }


    private void prepareStatements() {
        try {

            statements.put(TagStatement.ADD_TAG, this.connection.prepareStatement(
                    "insert into tag value (null, ?,?)"));

            statements.put(TagStatement.DELETE_TAG, this.connection.prepareStatement(
                    "delete from tag where id =?"));

            statements.put(TagStatement.EDIT_TAG, this.connection.prepareStatement(
                    "update tag  set label = ?, color =? where id=?"));

//            deleteTagBookStatement = connection.prepareStatement("deleteBookList from booktag where tag_id =?");

            statements.put(TagStatement.FIND_ALL_TAGS, this.connection.prepareStatement(
                    "select * from tag"));

            statements.put(TagStatement.FIND_ALL_TAGNAMES, this.connection.prepareStatement(
                    "select label from tag"));

            statements.put(TagStatement.FIND_TAG_BY_LABEL_COLOR, this.connection.prepareStatement(
                    "SELECT * from tag where (label,color) = (?,?)"));

            statements.put(TagStatement.JOIN_BOOK_BOOKTAG, this.connection.prepareStatement(
                    "select * from book b join booktag tb on b.id = tb.book_id \" +\n" +
                            "                    \"join Tag t  on tb.tag_id=t.id where t.id = ?"));

            statements.put(TagStatement.JOIN_TAG_BOOKTAG, this.connection.prepareStatement(
                    "select * from tag t join booktag tb on t.id = tb.tag_id join book b on b.id=tb.book_id where b.id =?"));

            statements.put(TagStatement.ADD_TO_BOOKTAG, this.connection.prepareStatement(
                    "insert into booktag values (?,?)"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addTag(Tag tag){
        PreparedStatement addTagStatement = statements.get(TagStatement.ADD_TAG);
        try {
            if(getTag(tag)==null) {
                addTagStatement.setString(1, tag.getLabel());
                addTagStatement.setString(2, tag.getColor().toString());
                addTagStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteTag(Tag tag){
        PreparedStatement deleteTagStatement = statements.get(TagStatement.DELETE_TAG);
        try {
            if (!(getTag(tag) == null)) {
                deleteTagStatement.setInt(1, getTag(tag).getId());
//                deleteTagBookStatement.setInt(1, getTag(tag).getId());
                deleteTagStatement.executeUpdate();
//                deleteTagBookStatement.executeUpdate();
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editTag(Integer tagId, Tag newTag){
        PreparedStatement editTagStatement = statements.get(TagStatement.EDIT_TAG);
        try {
                editTagStatement.setString(1, newTag.getLabel());
                editTagStatement.setString(2, newTag.getColor().toString());
                editTagStatement.setInt(3, tagId);
                editTagStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Tag> fetchAllTags(){
        List<Tag> tags = new ArrayList<>();
        PreparedStatement findAllTagsStatement = statements.get(TagStatement.FIND_ALL_TAGS);
        try {
            ResultSet resultSet = findAllTagsStatement.executeQuery();
            while (resultSet.next()){
                Tag tag = new Tag(resultSet.getInt(1),resultSet.getString(2),
                        Color.valueOf(resultSet.getString(3)));
                tags.add(tag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }

    public List<String> fetchAllTagsName(){
        List<String> tags = new ArrayList<>();
        PreparedStatement findAllTagNamesStatement = statements.get(TagStatement.FIND_ALL_TAGNAMES);
        try {
            ResultSet resultSet = findAllTagNamesStatement.executeQuery();
            while (resultSet.next()){
                String tagLabel = resultSet.getString(1);
                tags.add(tagLabel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }

    public Tag getTag(Tag tag){
        PreparedStatement findTagByNameColorStatement = statements.get(TagStatement.FIND_TAG_BY_LABEL_COLOR);
        try {
            findTagByNameColorStatement.setString(1, tag.getLabel());
            findTagByNameColorStatement.setString(2, tag.getColor().toString());
            ResultSet resultSet = findTagByNameColorStatement.executeQuery();
            if (resultSet.next()) {
                return new Tag(resultSet.getInt(1),
                        resultSet.getString(2),
                        Color.valueOf(resultSet.getString(3)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Ebook> filterBooksByTag(Tag tag){
        List<Ebook> books = new ArrayList<>();
        PreparedStatement findByTagStatement = statements.get(TagStatement.JOIN_BOOK_BOOKTAG);
        try {
            if (!(getTag(tag) == null)) {
                findByTagStatement.setInt(1, getTag(tag).getId());
                ResultSet resultSet = findByTagStatement.executeQuery();
                while (resultSet.next()) {
                    Ebook book = new Ebook(resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getInt(6),
                            resultSet.getString(7));
                    books.add(book);
                }
            }
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return books;
    }

    public List<Tag> findTagsbyBook(Integer bookid){
        List<Tag> tags = new ArrayList<>();
        PreparedStatement findTagByBookStatement = statements.get(TagStatement.JOIN_TAG_BOOKTAG);
        try {
            findTagByBookStatement.setInt(1, bookid);
                ResultSet resultSet =findTagByBookStatement.executeQuery();
                while(resultSet.next()) {
                    Tag tag = new Tag(resultSet.getInt(1), resultSet.getString(2),
                            Color.valueOf(resultSet.getString(3)));
                    tags.add(tag);
                }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return tags;
    }

    public void addToBookTag(Integer bookId, Tag tag) {
        PreparedStatement addToBookTagStatement = statements.get(TagStatement.ADD_TO_BOOKTAG);
        try {
            addToBookTagStatement.setInt(2, tag.getId());
            addToBookTagStatement.setInt(1, bookId);
            addToBookTagStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
