package model.tableObjects;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Ebook {
    private Integer id;
    private String title;
    private List<Author> authors;
    private String genre;
    private String publisher;
    private Integer edition;
    private Integer year;
    private String summary;
    private List<Comment> comments;
    private List<Tag> tags;


    public Ebook(){}

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", genre='" + genre + '\'' +
                ", publisher='" + publisher + '\'' +
                ", edition=" + edition +
                ", year=" + year +
                ", summary='" + summary + '\'' +
                ", comments=" + comments +
                ", tags=" + tags +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ebook ebook = (Ebook) o;
        return id.equals(ebook.id) &&
                title.equals(ebook.title) &&
                Objects.equals(authors, ebook.authors) &&
                Objects.equals(genre, ebook.genre) &&
                Objects.equals(publisher, ebook.publisher) &&
                Objects.equals(edition, ebook.edition) &&
                Objects.equals(year, ebook.year) &&
                Objects.equals(summary, ebook.summary) &&
                Objects.equals(comments, ebook.comments) &&
                Objects.equals(tags, ebook.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, authors, genre, publisher, edition, year, summary, comments, tags);
    }

    public Ebook(Integer id, String title, String genre, String publisher, Integer edition, Integer year, String summary) {
        this.id = id;
        this.title = title;
        this.authors = new ArrayList<>();
        this.genre = genre;
        this.publisher = publisher;
        this.edition = edition;
        this.year = year;
        this.summary = summary;
        this.comments = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(Author author) {
        this.authors.add(author);
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setEdition(Integer edition) {
        this.edition = edition;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setComments(Comment comment) {
        this.comments.add(comment);
    }

    public void setTags(Tag tag) {
        this.tags.add(tag);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getGenre() {
        return genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public Integer getEdition() {
        return edition;
    }

    public Integer getYear() {
        return year;
    }

    public String getSummary() {
        return summary;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<Tag> getTags() {
        return tags;
    }



}
