package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.ComboBoxItemWrap;
import model.Manager;
import model.dao.EbookDao;
import model.dao.TagDao;
import model.tableObjects.Comment;
import model.tableObjects.Ebook;
import model.tableObjects.Tag;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Manager manager;
    List<Ebook> inputList;
    static Integer counter=1;
    GridPane tagGridPane =new GridPane();
    List<Tag> tags = new ArrayList<>();
    ObservableList<Ebook> selectedBooks = FXCollections.observableArrayList();



    public MainController(){
        manager = new Manager();
        inputList = new ArrayList<>();
    }

    Stage authorWindow;
    Stage bookWindow;
    Integer tableId;

    // UI widgets
    @FXML
    private ComboBox TagComboBoxHome;
    @FXML
    private AnchorPane tagAnchorPaneHome;
    @FXML
    private TableView<Ebook> tableview;
    @FXML
    private TableColumn<String, Ebook> title;
    @FXML
    private TableColumn<String, Ebook> author;
    @FXML
    private TableColumn<String, Ebook> genre;
    @FXML
    private TableColumn<String, Ebook> publisher;
    @FXML
    private TableColumn<Integer, Ebook> edition;
    @FXML
    private TableColumn<Integer, Ebook> year;
    @FXML
    private TextArea summary;
    @FXML
    private TextField titleField;
    @FXML
    private TextField authorField;
    @FXML
    private TextField publisherField;
    @FXML
    private TextField genreField;
    @FXML
    private TextField yearField;
    @FXML
    private TextField serachtextField;
    @FXML
    private ComboBox<ComboBoxItemWrap<String>> authorFilterCombo;
    @FXML
    private ComboBox<?> tagFilterCombo;
    @FXML
    private VBox vboxComment;
    @FXML
    private Button OKButton_cmnt;
    @FXML
    private TextArea comment_area;

    SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy HH:mm");

    // ************ export & import ************
    @FXML
    void exportfile(MouseEvent  event) {
        selectedBooks = tableview.getSelectionModel().getSelectedItems();
        List<Ebook> listForExport = new ArrayList<Ebook>(selectedBooks);
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export");
            fileChooser.setInitialFileName("Export");
            File savedFile = fileChooser.showSaveDialog(form);
            if (savedFile != null) {
                System.out.println(savedFile.getParent() + " - " + savedFile.getName());
                manager.exportBooks(listForExport,savedFile.getParent() + "\\", savedFile.getName());
            }else {
                System.out.println("Export cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @FXML
    void importfile(MouseEvent  event) {
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Import");
            File savedFile = fileChooser.showOpenDialog(form);
            if (savedFile != null) {
                System.out.println(savedFile.getParent()+ " - " + savedFile.getName().replaceAll(".zip",""));
                manager.importBooks(savedFile.getParent()+"\\", savedFile.getName().replaceAll(".zip",""));
                refreshTable();
            }
            else {
                System.out.println("Import cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // ************ add book ************
    @FXML
    void addBook(MouseEvent event) {
            bookWindow = new Stage();
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/forms/addNewBookForm.fxml"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            bookWindow.setScene(new Scene(root));
            bookWindow.show();
            bookWindow.setOnHiding( event1 -> {
                System.out.println("Stage is closing");
                refreshTable();
            });
    }

    // ************ deleteBookList book ************
    @FXML
    void deleteBookList(MouseEvent event) {
        selectedBooks = tableview.getSelectionModel().getSelectedItems();
        List<Ebook> listForDelete = new ArrayList<Ebook>(selectedBooks);
        manager.deleteBookList(listForDelete);
        refreshTable();
    }

    // ************ edit book ************
    @FXML
    void editBook(MouseEvent event) {
        Ebook ebook = inputList.get(tableId);
        ebook.setTitle(titleField.getText());
        ebook.setSummary(summary.getText());
        ebook.setPublisher(publisherField.getText());
        ebook.setYear(Integer.parseInt(yearField.getText()));
        ebook.setGenre(genreField.getText());
        manager.editBook(ebook);
        addTagsafterEditPressed();
        refreshTable();
        clearFields();
    }

    // ************ display ************
    @FXML
    void tableSelection(MouseEvent event) {
        if(tableview.getSelectionModel().getSelectedIndex()>=0) {
            tableview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            tableId = tableview.getSelectionModel().getSelectedIndex();
            titleField.setText(inputList.get(tableId).getTitle());
            publisherField.setText(inputList.get(tableId).getPublisher());
            yearField.setText("" + inputList.get(tableId).getYear());
            genreField.setText(inputList.get(tableId).getGenre());

            TableRow<Ebook> row = new TableRow<>();
            String bookName = "";
            if ((event.getClickCount() == 2) ) {
                bookName = inputList.get(tableId).getTitle();
                System.out.println("Opening PDF file in external reader : " + bookName);
                manager.openPDF(bookName);
            }
        }else{
            System.out.println("Nothing exist in the table");
        }
        showTagsOfBook();
//        handleComments(inputList.get(tableId).getId());
    }

    @FXML
    void refreshTable(MouseEvent event) {
        refreshTable();
    }

    public void refreshTable(){
        mainTable(getInputList());
        clearFields();
    }

    public ObservableList<Ebook> getInputList(){
        inputList = manager.fetchEbooks();
        ObservableList<Ebook> inputList2 = FXCollections.observableArrayList(inputList);
//        for(Ebook ebook:inputList){
//            inputList2.storeBook(ebook);
//        }
        return inputList2;
    }

    public void mainTable(ObservableList<Ebook> inputList) {
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        edition.setCellValueFactory(new PropertyValueFactory<>("edition"));
        year.setCellValueFactory(new PropertyValueFactory<>("year"));
        genre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        publisher.setCellValueFactory(new PropertyValueFactory<>("publisher"));
        tableview.setItems(inputList);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearFields();
        refreshTable();
        initAuthorComboBox();
    }

    private void clearFields() {
        titleField.clear();
        authorField.clear();
        publisherField.clear();
        genreField.clear();
        yearField.clear();
        summary.clear();
    }

    // ************ filter ************

    public void initAuthorComboBox(){

        authorFilterCombo.setPromptText("Filter");

        authorFilterCombo.setCellFactory( c -> {
            ListCell<ComboBoxItemWrap<String>> cell = new ListCell<>(){
                @Override
                protected void updateItem(ComboBoxItemWrap<String> item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        final CheckBox cb = new CheckBox(item.toString());
                        cb.selectedProperty().bind(item.getCheckProperty());
                        setGraphic(cb);
                    }
                }
            };

            cell.addEventFilter(MouseEvent.MOUSE_RELEASED, event2 -> {
                cell.getItem().getCheckProperty().setValue(!cell.getItem().getCheckProperty().getValue());
            });
            return cell;
        });
    }

    @FXML
    void ShowTags(MouseEvent event) {
        ObservableList myTags = FXCollections.observableArrayList(manager.fetchAllTagsName());
        tagFilterCombo.setItems(myTags);
    }

    @FXML
    void ShowAuthors(MouseEvent event) {
        List<String> authorNames  = manager.fetchAllAuthorNames();
//        System.out.println(authorNames);

        List<String> authorNamesTemp = new ArrayList<>();
        for (ComboBoxItemWrap<String> author: authorFilterCombo.getItems()) {
            authorNamesTemp.add(author.getItem());
        }

        authorNames.removeAll(authorNamesTemp);
        for (String author: authorNames)
            authorFilterCombo.getItems().add(new ComboBoxItemWrap<>(author));
    }

    @FXML
    void FilterBook(MouseEvent event) throws Exception {

        List<String> authorList = new ArrayList<>();
        List<String > tagList = new ArrayList<>();
        List<Ebook> filteredEbooks = new ArrayList<>();

        authorFilterCombo.getItems().filtered( f ->
                                        f.getCheck()).forEach( item -> {
                                            authorList.add(item.getItem());
                                            System.out.println("selected: "+item.getItem());}
                                            );

//        String tagValue = (String) tagFilterCombo.getValue();
//        tagList.add(tagValue);

        if(!authorList.isEmpty() && !tagList.isEmpty())
            filteredEbooks = manager.filterByAuthorTagList(authorList, tagList);
        else if(authorList.isEmpty() && !tagList.isEmpty())
            filteredEbooks = manager.filterByTagList(tagList);
        else if(!authorList.isEmpty() && tagList.isEmpty())
            filteredEbooks = manager.filterByAuthorList(authorList);


        tableview.getItems().clear();
        ObservableList<Ebook> inputList = FXCollections.observableArrayList(filteredEbooks);
        mainTable(inputList);

        }

    // ************ tag ************
    public void addTagsafterEditPressed(){
//        AddTagToBook.tagsOfBookList.forEach(System.out::println);
        TagDao tagDao =new TagDao(new Manager().getConnection());
//        for(Tag tag:AddTagToBook.tagsOfBookList){
//            tagDao.addToBookTag(tag.getId(), inputList.get(tableId).getId());
//        }
    }

    @FXML
    void addTag(MouseEvent event) {

        bookWindow = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/forms/addNewTagForm.fxml"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
            bookWindow.setScene(new Scene(root));
            bookWindow.show();
    }

    @FXML
    void initializeTagComboBoxHome(MouseEvent event) {
        TagComboBoxHome.getItems().clear();
        TagDao tagDao = new TagDao(manager.getConnection());
        for(Tag tag:tagDao.fetchAllTags()){
            Label tagItem = new Label(tag.getLabel());
            tagItem.setTextFill(tag.getColor());
            TagComboBoxHome.getItems().add(tagItem);
    }

    }

    public void showTagsOfBook(){
        //tagGridPane.getChildren().clear();
        new ShowTagsofBook(tagGridPane,inputList.get(tableId).getId(), counter,tags);

        if(!tagAnchorPaneHome.getChildren().contains(tagGridPane)) {
            tagAnchorPaneHome.getChildren().add(tagGridPane);
        }

    }

    @FXML
    void addTagtoBookHome(MouseEvent event) {

//        new AddTagToBook(TagComboBoxHome, tagGridPane,tags);
    }

    // ************ search ************
    @FXML
    void searchClicked(MouseEvent event) {
        if(!serachtextField.getText().trim().isEmpty()){

            ObservableList<Ebook> searchBooks = FXCollections.observableArrayList(
                    new EbookDao(manager.getConnection()).search(serachtextField.getText()));
            tableview.getItems().clear();
            mainTable(searchBooks);
        }
    }

    // ************ comment ************
    public void handleComments(Integer bookId){

        // show comments on tab selection in MainForm
        List<Comment> comments = inputList.get(tableId).getComments();
        System.out.println("book_id: "+inputList.get(tableId).getId());
        System.out.println("size: "+inputList.get(tableId).getComments().size());
        System.out.println();

        vboxComment.getChildren().clear();
        OKButton_cmnt.setOnMouseClicked(e-> {
            try{

                Comment c = new Comment(null, comment_area.getText(), new Date(), inputList.get(tableId).getId());
//                    comments.storeBook(c);

                TextField tf = new TextField(comment_area.getText());
                Button edit = new Button("Edit");
                Button delete = new Button("Delete");
                Label dateLbl = new Label(dateFormat.format(c.getDate()));

                comment_area.clear();
                HBox row = new HBox();
                row.getChildren().addAll(dateLbl, tf, edit, delete);
                vboxComment.getChildren().add(row);
                inputList.get(tableId).getComments().add(c);
                manager.addComment(inputList.get(tableId).getId(), c);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });


        for (Comment comment: inputList.get(tableId).getComments()) {
            TextField tf = new TextField(comment.getContent());
            Button edit = new Button("Edit");
            Button delete = new Button("Delete");
            Label dateLbl = new Label(dateFormat.format(comment.getDate()));

            HBox row = new HBox();
            row.getChildren().addAll(dateLbl, tf, edit, delete);
            vboxComment.getChildren().add(row);

            edit.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    try {

                        Stage form = new Stage();
                        FXMLLoader fx = new FXMLLoader(getClass().getResource("/forms/editCommentForm.fxml"));
                        Parent parent = fx.load();

                        //Get view of next scene
                        EditCommentController editCommentController = fx.getController();
                        //show current comment in next scene's textArea
                        editCommentController.setComment(comment.getContent());

                        form.setScene(new Scene(parent));
                        form.showAndWait();

                        //update comment's date and content
                        comment.setContent(editCommentController.getComment());
                        comment.setDate(new Date());
                        tf.setText(comment.getContent());

                        manager.editComment(comment);


                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                }

            });

            delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
//                    comments.remove(comment);
                    manager.deleteComment(comment.getBookId());
                    inputList.get(tableId).getComments().remove(comment);
                    vboxComment.getChildren().remove(row);

                }
            });
//
        }
    }
}
