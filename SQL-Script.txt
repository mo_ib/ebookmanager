create table author
(
    id   int auto_increment primary key,
    name varchar(60) not null,
    unique (name)
);

create table book
(
    id        int auto_increment primary key,
    title     varchar(60) not null,
    genre     varchar(40) null,
    publisher varchar(50) null,
    edition   int         null,
    year      year        null,
    summary   text        null,
    unique (title, edition)
);

create table bookauthor
(
    book_id      int not null,
    author_id    int not null,
    constraint bookauthor_ibfk_1
        foreign key (book_id) references book (id)
            on delete cascade,
    constraint bookauthor_ibfk_2
        foreign key (author_id) references author (id)
            on delete cascade,
    author_order int not null,
    primary key (book_id, author_id),
    unique (book_id, author_id)
);

create table tag
(
    id    int auto_increment primary key,
    label varchar(50) not null,
    color varchar(30) null,
    unique (label)
);

create table booktag
(
    book_id int not null,
    constraint booktag_ibfk_1
        foreign key (book_id) references book (id)
            on delete cascade,
    tag_id  int not null,
    constraint booktag_ibfk_2
        foreign key (tag_id) references tag (id)
            on delete cascade,
    primary key (book_id, tag_id)
);

create table comments
(
    id      int auto_increment primary key,
    content text not null,
    date    date null,
    book_id int,
    constraint comments_ibfk_1
        foreign key (book_id) references book (id)
            on delete cascade
);
